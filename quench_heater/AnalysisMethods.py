import numpy as np
from scipy import signal as s
import pandas as pd
from scipy.optimize import leastsq
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.spatial.distance import euclidean, pdist, squareform
from scipy import signal as s
import operator

from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets

# Internal packages
from lhcsmapi.Time import Time
from lhcsmapi.Timer import Timer
from lhcsmapi.dbsignal.SignalAnalysis import SignalAnalysis
from lhcsmapi.dbsignal.SignalElectrical import SignalElectrical
from lhcsmapi.dbsignal.ExponentialSignalAnalysis import ExponentialSignalAnalysis
from lhcsmapi.dbsignal.SignalProcessing import SignalProcessing
from lhcsmapi.dbsignal.Signal import Signal
from lhcsmapi.post_mortem.PmDbRequest import PmDbRequest
from lhcsmapi.metadata.Metadata import Metadata

class AnalysisMethods:
    
    def getCommonDF(fdata,refData, printdifference = False):
        """ Method merges the dataframe by timestamp and magnet """
        commonData = pd.merge(fdata, refData, how='inner', on = ['timestamp','magnet'])
        if printdifference == True:
            print(str(len(commonData)) + " common events: " + str(len(refData)-len(commonData)) + " not in my data; " + str(len(fdata)-len(commonData)) + " not in LabVIEW data")    

        AnalysisMethods.getStatusFromRefData(commonData)
        return commonData

    def getStatusFromRefData(data, source = "Intersection with LabVIEW labels: "):
        """ Method prints number of OK, NOT_OK of given DF"""
        good=len(data[data['status']=='OK'])
        bad=len(data[data['status']=='NOT_OK'])   
        print(source + str(good+bad) + " Discharges "  + " OK:"+ str(good)+ " NOT_OK:"+ str(bad))

    def applyLogicOperationOnIndices(df, signalNames, suffix, threshold, index, comparisonOperator, logicOperation):
        """ Method applies logic operation on indices and returns index """
        for signalName in signalNames:
            signalNameMax = signalName + suffix        
            comparisonResult = comparisonOperator(df[signalNameMax], threshold)        
            foundIndices = comparisonResult[comparisonResult.values == True].index
            index = logicOperation(index, foundIndices)
        return index

    def applyFeatureComparisonOnIndices(df, signalNames, suffix1, deviationAllowance , suffix2, index , comparisonOperator, logicOperation):
        """ Method compares two columns of given DF with given comparison operator and returns index """
        for signalName in signalNames:
            signalName1 = signalName + suffix1
            signalName2 = signalName + suffix2

            comparisonResult = comparisonOperator(df[signalName1], df[signalName2]+deviationAllowance) 

            foundIndices = comparisonResult[comparisonResult.values == True].index
            index = logicOperation(index, foundIndices)
        return index
    
    @staticmethod
    def getFrequencyAnalysis(originSeries):
        """ Function calculates and plots spectrogram and returns the filterbank features from the spectrogram """

        f, t, Sxx = SignalAnalysis.calculateSpectrogramFromSeries(originSeries)

        fig , ax = plt.subplots(figsize=(15,5))
        ax.plot(originSeries.index,originSeries.values)
        plt.title(originSeries.name)
        plt.xlabel('Time [s]')
        plt.xlim(originSeries.index[0],originSeries.index[-1])
        plt.ylabel('\n \n Current [A]')

        plt.figure(figsize=(19,5))
        plt.pcolormesh(t, f, Sxx, norm=colors.LogNorm(vmin=Sxx.min(), vmax=Sxx.max()))
        plt.title(originSeries.name + "Spectrogram")
        plt.ylabel('Frequency [Hz]')
        plt.xlabel('Time [sec]')
        plt.colorbar()
        plt.show() 

        filterBankMaxValues = SignalAnalysis.calculateFilterBankFromSpectrogram([f, t, Sxx])  
        return filterBankMaxValues
    
    @staticmethod
    def getHistogramBinsOutofRange(myDF,settling = 2000,sigmaMultiplicator = 3,histrange=[-100, 3000]):
        """ Function counts all bins which are out of range sigmaMultiplicator*std """
        dR= myDF.filter(regex=("dR_HDS_\d{1}"))
        outliersSum = []
        for name in dR.columns.values:
            v = myDF[name].values
            fig = plt.figure()
            x = plt.hist(v[settling:],100,range=histrange)
            plt.close(fig)
            sigma = np.std(v)

            bins = x[0]
            outliersBinary = x[1]>(sigmaMultiplicator*sigma+50) #offset if whole dR dataframe is with 0
            outliersSum += [sum(bins[outliersBinary[1:]])]

        newFrame = pd.DataFrame(data = outliersSum, index = [dR.columns.values], columns = [dR.index[0]])

        return(newFrame.T)
    
    def fetchCalsOperatingCurrent(circuitType, magnet,timestamp):
        """ Function operating current from cals database """
        arcName = Metadata.getArcNamesFromMagnetNames(circuitType, magnet)
        arcMetadata = Metadata.getMetadata(circuitType, arcName)
        arcCurrentName = arcMetadata[arcName]['signalFGC_ABS']
        duration = [(10, 's'), (10, 's')]
        try:
            signal = Signal().read('cals', name=arcCurrentName, startTime=timestamp, duration=duration, ldb = ldb)
            return SignalProcessing.findValueWithTimeClosestToIndexThreshold(signal[arcCurrentName], timestamp)
        except:
            return None
            print("\tCouldn't query CALS: {} at {} for {}".format(arcCurrentName, timestamp, duration))
            
    def fetchPMOperatingCurrent(circuitType,systemType,magnetType, magnet,timestamp,eventDurationSeconds):
            selectedDayTimeStamp = Time.convertToString(timestamp-eventDurationSeconds/2) #look 10min into the past
            magnetNameToArc = Metadata.createMetadata(circuitType)["REL_IP_POS_TO_CIRCUIT_NAME"]
            sectors = set(magnetNameToArc.values())

            magnetTypeToSignalMetadata = Metadata.getMetadata(circuitType, systemType)
            fgcMetadata = magnetTypeToSignalMetadata[systemType]
            system = fgcMetadata["PM"]['system']
            className = fgcMetadata["PM"]['className']

            eventSourceAndTimestamp = []
            for sector in sectors:
                arcToSignalMetadata = Metadata.getMetadata(circuitType, sector)
                metadata = arcToSignalMetadata[sector]
                source = metadata['PC_NAME']
                eventSourceAndTimestamp += PmDbRequest.findEvents(source, system, className, 
                                                                  startTime = selectedDayTimeStamp, 
                                                                  duration = [(eventDurationSeconds, 's')])

            if eventSourceAndTimestamp: 
                source, timestamp = eventSourceAndTimestamp[0]
                magnetTypeToSignalMetadata = Metadata.getMetadata(component, magnetType)
                metadata = magnetTypeToSignalMetadata[magnetType]
                signalNames = metadata['iNames']

                frameName = component
                FgcSignal = Signal()
                for signalName in signalNames:
                    FgcSignal.read('pm',append=True, system=system, className=className, 
                                            source=source, signalName=signalName, eventTime=timestamp)

                return np.mean(s.medfilt(FgcSignal.df.values[0:19],3))
            else: 
                print("\tNo PM opperating current found at {} for {}s".format(timestamp, eventDurationSeconds))    

    def plotSignalAndReferenceAndDifference(plotDF,k, normalizeSignals = True):
        if len(plotDF) > 0: 
            magnet = plotDF['magnet'].iloc[k]
            timestamp  = plotDF['timestamp'].iloc[k]

            magnetType = 'MB'
            circuitType = 'RB'
            component = 'QPS'
            metadata = Metadata.getMetadata(component, magnetType)[magnetType]
            system = metadata['system']
            className = metadata['className']
            uNames = metadata['uNames']
            iNames = metadata['iNames']
            rNames = metadata['rNames']

            print('LabVIEW classified this signal as: '+ plotDF['status'].iloc[k])
            print('Analysis_cobermai classified this signal as: '+ plotDF['myStatus'].iloc[k])
            print('Analysis_cobermai classification reason: '+ plotDF['myStatusInformation'].iloc[k])
            if 'manualStatus' in plotDF.columns.values:
                print('Overall, this signal was classified as: '+ str(plotDF['manualStatus'].iloc[k]))

            QhDischarge = Signal()
            for signalName in uNames+iNames:
                QhDischarge.read('pm', append=True, system=system, className=className, source=magnet, 
                                      signalName=signalName, eventTime=int(timestamp))

            myDF = QhDischarge.getSynchronizedTimeDataframe()
            if 'decayStartTime' in plotDF.columns.values:
                myDF = myDF[myDF.index > plotDF['decayStartTime'].iloc[k]*1e9]
            SignalElectrical.calculateResistances(myDF, uNames, iNames, rNames)

            myDFDecayContinuous = SignalAnalysis.resampleSeries(myDF,ts=83328).rolling(window=50).median()
            newIndex = [Time.convertToUnixTimeSeconds(t) for t in myDFDecayContinuous.index.tolist()]
            myDFDecayContinuous.set_index([newIndex], inplace=True)    

            #Query reference signal
            refTimestamp = np.int64(plotDF['timestamp_Ref'].iloc[k])
            QhDischargeRef = Signal()
            for signalName in uNames+iNames:
                QhDischargeRef.read('pm', append=True, system=system, className=className, source=magnet, 
                                      signalName=signalName, eventTime=refTimestamp)
            myDFRef = QhDischargeRef.getSynchronizedTimeDataframe()
            if 'decayStartTime' in plotDF.columns.values:
                myDFRef = myDFRef[myDFRef.index > plotDF['decayStartTime_Ref'].iloc[k]*1e9] 

            SignalElectrical.calculateResistances(myDFRef, uNames, iNames, rNames)   

            myDFDecayContinuousRef = SignalAnalysis.resampleSeries(myDFRef,ts=83328).rolling(window=50).median() 
            newIndex = [Time.convertToUnixTimeSeconds(t) for t in myDFDecayContinuousRef.index.tolist()]
            myDFDecayContinuousRef.set_index([newIndex], inplace=True)  
            
            envelopeAmplitude = [20,5,0.5]
            #Normalize Signals    
            if normalizeSignals:
                normalizationCoefficient = [1/(np.max(myDFDecayContinuous[signalName].dropna().values)-np.min(myDFDecayContinuous[signalName].dropna().values)) for signalName in [uNames,iNames,rNames]]              
                myDFDecayContinuous = AnalysisMethods.getNormalizedDFofSignalGroup(myDFDecayContinuous,[uNames,iNames,rNames])
                myDFDecayContinuousRef = AnalysisMethods.getNormalizedDFofSignalGroup(myDFDecayContinuousRef,[uNames,iNames,rNames])     
                envelopeAmplitude = (np.array(normalizationCoefficient)*np.array(envelopeAmplitude)).tolist()

            #Substract signals
            myDFSubstract = AnalysisMethods.substactDFWithDifferentLength(myDFDecayContinuous,myDFDecayContinuousRef) 

            AnalysisMethods.uirPlot(myDFDecayContinuous, "Signal ", magnet, timestamp)
            AnalysisMethods.uirPlot(myDFDecayContinuousRef, "Reference Signal ", magnet, refTimestamp)  
            AnalysisMethods.uirPlot(myDFSubstract, 'Substracted Signals ', magnet, timestamp,envelopeAmplitude)   
            
        else: print('No data within this filter settings')

    def showInteractivePlot(plotDF,normalizeSignals = True):
        def queryMagnet(k):
            AnalysisMethods.plotSignalAndReferenceAndDifference(plotDF,k,normalizeSignals)
        interactive_plot = interactive(queryMagnet, k=(0, len(plotDF)-1))
        output = interactive_plot.children[-1]
        return interactive_plot

    def uirPlot(myDF, title, magnet, timestamp, envelopeAmplitude = False):
            magnetType ='MB'
            circuitType = 'RB'
            component = 'QPS'
            metadata = Metadata.getMetadata(component, magnetType)[magnetType]
            system = metadata['system']
            className = metadata['className']
            uNames = metadata['uNames']
            iNames = metadata['iNames']
            rNames = metadata['rNames']

            fig, ax = plt.subplots(1, 3, figsize=(25, 5))
            myDF.filter(uNames).plot(ax=ax[0]).set(title  = (title +' '+ magnet +' '+ Time.convertToString(timestamp, unit='ns')) ,xlabel="[s]", ylabel="[V]") #
            myDF.filter(iNames).plot(ax=ax[1]).set(title  = (title +' '+magnet +'  '+ Time.convertToString(timestamp, unit='ns')), xlabel="[s]", ylabel="[A]")
            myDF.filter(rNames).plot(ax=ax[2]).set(title =(title +' '+magnet +'  '+ Time.convertToString(timestamp, unit='ns')),xlabel="[s]", ylabel="[Ohm]")

            if envelopeAmplitude: 
                #Fixed time constant only to make ploting faster here, usually it is the timeconstant of the reference signal
                pd.DataFrame(np.exp(-myDF.index/0.18)*envelopeAmplitude[0],myDF.index,['Envelope']).plot(ax=ax[0],linestyle = '--')
                pd.DataFrame(np.exp(-myDF.index/0.18)*envelopeAmplitude[1],myDF.index,['Envelope']).plot(ax=ax[1],linestyle = '--')
                pd.DataFrame(np.exp(-myDF.index/0.18)*envelopeAmplitude[2],myDF.index,['Envelope']).plot(ax=ax[2],linestyle = '--')

    def substactDFWithDifferentLength(mySeriesDecay,mySeriesDecayRef): #,signalNames, envelopeAmplitude,envelopeDecayTimeConstant):
        #for signalName in signalNames:

        end = np.min([len(mySeriesDecay),len(mySeriesDecayRef)])
        mySeriesDecay = mySeriesDecay.iloc[0:end]
        mySeriesDecayRef = mySeriesDecayRef.iloc[0:end]

        substractArray = np.abs(mySeriesDecayRef.values-mySeriesDecay.values)
        return pd.DataFrame(substractArray, mySeriesDecay.index,mySeriesDecay.columns)
    
    def calculateDifferenceToReferences(df,featureRegex):
        for feature in featureRegex:
            signalNames = df.filter(regex = feature + '$').columns.values
            for signalName in signalNames:
                df[signalName + "_dif"] = df[signalName] - df[signalName+ "_Ref"]
        return df

    def calculateMeanOfFeaturesAndCutOutiers(filteredData,regex,features,margins = False, takeAbsoluteValues = True): 
        for feature,margin  in zip(features,margins):   
            if takeAbsoluteValues: filteredData[feature+'_mean'+regex[0]] = filteredData.filter(regex = (regex + feature + '$')).abs().mean(axis = 1) 
            else: filteredData[feature+'_mean'+regex[0]] = filteredData.filter(regex = (regex + feature + '$')).mean(axis = 1) 
            if margins:
                filteredData[feature+'_mean'+regex[0]].values[filteredData[feature+'_mean'+regex[0]] >  margin] =  margin
                filteredData[feature+'_mean'+regex[0]].values[filteredData[feature+'_mean'+regex[0]] <  -margin] =  -margin
        return filteredData
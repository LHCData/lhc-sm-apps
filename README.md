# LHC SIGNAL MONITORING PROJECT
<center><img src = "https://gitlab.cern.ch/LHCData/lhc-sm-apps/raw/master/figures/logo.png" width=25%></center>

LHC is a complex machine composed of many interconnected systems serving a broad scientific community. Although its performance is a considerable achievement, there have been already several cases of hardware deterioration .  Early detection of fault precursors reduces fault severity and facilitates maintenance planning. To this end, LHC undergoes a rigorous testing during the Hardware Commissioning . However, LHC systems are not extensively monitored during operation. The signals representing LHC systems have been continuously logged during the operation. As a result,  there is a lot of data in terms of volume and variety which can be used to  gather information about reference values of signals for the most important systems. Signal features engineered this way will be of high importance to serve as a feedback on the performance evaluation design decisions and will support development of accelerators.

The LHC Signal Monitoring project aims at developing software capable of monitoring signals in order to detect deviation from standard operating values. In other words, we aim at introducing predictive maintenance capabilities for LHC. The project scope includes but is not limited to
- Superconducting magnets and busbars
- Power Converters
- Current leads
- Grounding networks
- Circuit and magnet protection systems

<img src = "https://gitlab.cern.ch/LHCData/lhc-sm-apps/raw/master/figures/scope.png" width=50%>

## Project overview
The project is composed of two layers and reflects two development avenues: (i) development of use cases for signal analysis as well as metadata representing circuit and signal names; (ii) development of applications for signal analysis and eventually on-line monitoring.

The bottom layer consists of an API and metadata modules. The API supports read access for signals stored in Post Mortem and CALS databases (NXCALS is being integrated) as well as write access of intermediate results to an InfluxDb database.

The upper layer makes use of the aforementioned modules and in order to perform query of various signals and events occurring in the superconducting circuits of the LHC.

<img src = "https://gitlab.cern.ch/LHCData/lhc-sm-apps/raw/master/figures/architecture.png" width=75%>

## Methodology
In order to perform monitoring of signals representing the operational states of LHC systems, the applied methodology distinguishes different execution modes, analysis types and reference values. The monitoring can be executed continuously or linked to a specific event in the machine. The features extracted from signals can have either a generic or more problem-specific character. Eventually, these features serve as reference values for the signal monitoring.

<i>Execution Mode</i>
- Continuous
- Event-driven

<i>Analysis Type</i>
- Raw signals
- simple features: average, max, min, mean, std, etc.
- advanced features: trends, characteristic time, correlation, frequency spectrum

<i>Reference Values</i>
- Prescribed threshold
- Past data from the same circuit
- Data from similar circuits (correlations)
- Pattern recognition (machine learning)
- Existing models of electrical circuits (digital twin)

Applications developed within the scope of the project correspond to three phases of signal analysis: Acquisition -> Exploration -> Monitoring.
<img src = "https://gitlab.cern.ch/LHCData/lhc-sm-apps/raw/master/figures/methodology.png" width=75%>

### Acquisition
The goal of the Acquisition phase is to acquire signals and extract relevant features characterising the considered signals. This phase is typically divided into two phases: (i) analysis of a single signal (often times related to a single event in the machine); (ii) extraction of these features for a larger span of machine operation.

|              | Quench heaters                                               | Busbar resistance         | Earth current           | Voltage feelers | Diode lead resistance |
|--------------|--------------------------------------------------------------|---------------------------|-------------------------|-----------------|-----------------------|
| Circuit      | RB, RQ, RQX, IPQD2/4/8, IPD2                                 | RB, RQ                    | RB, RQ, 600 A           | RB, RQ          | RB                    |
| Event        | quench                                                       | plateau                   | FPA                     | ramp-up FPA     | quench                |
| Event filter | event name initial and final charge                          | plateau duration          | I_MEAS>1 kA             | I_MEAS>1 kA     | I_MEAS>1 kA           |
| Features     | min, max, start, end characteristic time, frequency spectrum | Resistance: SNR, avg, std | I_MEAS: min, max, final | Similarity      | R: SNR, avg, std      |

<i>Beam mode filtering</i>

This module extracts beam mode indices corresponding to current ramp-up from the injection through the stable beams.
* [BmManyDays](beam_mode/BmManyDays.ipynb) - for extraction of beam mode parameters over a certain period of time.

<i>Quench heater</i>

This analysis module is suited for querying quench heater signals during discharge, i.e., voltage and current (for main dipole magnets).
* [QhSingleDay](quench_heater/QhSingleDay.ipynb) - for analysis of a quench heater discharge selected during a single day

<i>Power converter</i>
* [FgcSingleDay](power_converter/FgcSingleDay.ipynb) - for analysis of a single Post Mortem dump of a power converter selected during a single day
* [FgcManyDays](power_converter/FgcManyDays.ipynb) - for analysis of many Post Mortem dumps of a power converter in a given period of time

<i>Earth current</i>
* [EcManyFpas](earth_current/EcManyFpas.ipynb) - for analysis of the grounding network during an FPA in a given period of time

<i>Voltage feeler</i>
* [VfSingleRamp](voltage_feeler/VfSingleRamp.ipynb) - for analysis of the voltage feeler behavior during a single current ramp-up
* [VfManyRamps](voltage_feeler/VfManyRamps.ipynb) - for analysis of the voltage feeler behavior during many current ramp-ups

<i>Busbar</i>

This module calculates busbar and magnet resistances corresponding to a current ramp-up.
* [BbSingleRamp](busbar/BbSingleRamp.ipynb) - for analysis of busbar/magnet resistance during a single current ramp-up
* [BbManyRamps](busbar/BbManyRamps.ipynb) - for analysis of busbar/magnet resistance during many current ramp-ups

<i>Diode lead resistance</i> - Work in progress.

### Exploration
The goal of the Exploration phase is to filter out noise measurement in order to obtain the reference values from the past as well as detect potential failures. The analysis modules available in the Exploration phase make use of signals and their features calculated in the Acquisition.

<i>Quench heaters</i>
- [QPS_StatisticalAnalysis](quench_heater/QPS_StatisticalAnalysis.ipynb)

<i>Busbar resistance</i> - Work in progress.
- [StatisticDashboard_BusbarResistanceCalculation](busbar_resistance/StatisticDashboard_BusbarResistanceCalculation.ipynb)

### Monitoring
The Monitoring phase will consist of automated routines to track signals of selected equipment in the LHC. The signals and their features will be compared to reference values calculated in the Exploration phase. In addition, there will be modules detecting precursors of past failures that occurred in the LHC.

<i>Quench heaters</i> - Work in progress.

## How to use the project
#### API and Metadata
In order to use the project, API has to be installed with a python package installer as

```python
pip install --user lhcsmapi
```
Check the latest version at [https://pypi.org/project/lhcsmapi/](https://pypi.org/project/lhcsmapi/)

The API relies on external python packages which have to be installed in a similar manner. The list of packages is stored in the <u><i>requirements.txt</i></u> file.

The documentation for the API is stored at [http://cern.ch/lhc-sm-api](http://cern.ch/lhc-sm-api).

The GitLab repository of the API is stored at [http://gitlab.cern.ch/lhcdata/lhc-sm-api](http://gitlab.cern.ch/lhcdata/lhc-sm-api)

#### Applications
The released use cases are available at the [SWAN gallery](http://swan.web.cern.ch/content/large-hadron-collider-lhc-monitoring).

The beta versions of the use cases are versioned at a GitLab repository [http://gitlab.cern.ch/lhcdata/lhc-sm-apps](http://gitlab.cern.ch/lhcdata/lhc-sm-apps)

Use with SWAN
1. Go to SWAN https://swan.cern.ch
  * In case you haven’t activated CERNbox yet, go to https://cernbox.cern.ch
2. Download a selected notebook from https://gitlab.cern.ch/lhcdata/lhc-sm-apps
3. Create a directory in your SWAN space
4. Upload the notebook
5. Execute the notebook, cell by cell

import ipywidgets as widgets
from ipywidgets import Layout
from ipywidgets import Box
import functools
from lhcsmapi.Time import Time

class StartEndTimeModule(object):
    def __init__(self, start_time='2018-12-12 00:00:00', end_time='2018-12-13 00:00:00'):
        placeholder = 'YYYY-MM-DD HH:MM:SS'
        self.txt_start = widgets.Text(value=start_time, placeholder=placeholder, description='Start time:')
        self.txt_start_value = start_time
        self.txt_start.on_submit(functools.partial(StartEndTimeModule.update_txt_start, self=self))
        self.txt_start_event_handler = None
        
        self.txt_end = widgets.Text(value=end_time, placeholder=placeholder, description='End time:')
        self.txt_end_value = end_time
        self.txt_end.on_submit(functools.partial(StartEndTimeModule.update_txt_end, self=self))
        self.txt_end_event_handler = None
        
        self.lbl_status = widgets.Label(value="")
        self.widget = Box(children = [self.txt_start, self.txt_end, self.lbl_status])
    
    @staticmethod
    def check_time(start_time, end_time):
        start_time_unix = Time.convertToUnixTimestamp(start_time)
        end_time_unix = Time.convertToUnixTimestamp(end_time)
        return start_time_unix < end_time_unix
    
    @staticmethod
    def update_txt_start(value, self):
        if StartEndTimeModule.check_time(self.get_start_time(), self.get_end_time()):
            self.lbl_status.value = ''
            self.txt_start_value = self.txt_start.value
            if self.txt_start_event_handler is not None:
                self.txt_start_event_handler()
        else:
            self.lbl_status.value = 'The start date is wrong, please type it again'
            self.txt_start.value = self.txt_start_value
        
            
    @staticmethod
    def update_txt_end(value, self):
        if StartEndTimeModule.check_time(self.get_start_time(), self.get_end_time()):
            self.lbl_status.value = ''
            self.txt_end_value = self.txt_end.value
            if self.txt_end_event_handler is not None:
                self.txt_end_event_handler()
        else:
            self.lbl_status.value = 'The end date is wrong, please type it again'
            # set end time to empty
            self.txt_end.value = self.txt_end_value
        
    def add_txt_start_event_handler(self, _txt_start_event_handler):
        self.txt_start_event_handler = _txt_start_event_handler
        
    def add_txt_end_event_handler(self, _txt_end_event_handler):
        self.txt_end_event_handler = _txt_end_event_handler
        
    def get_widget(self):
        return self.widget
    
    def get_start_time(self):
        return self.txt_start.value
    
    def get_end_time(self):
        return self.txt_end.value
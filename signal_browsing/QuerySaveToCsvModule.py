import ipywidgets as widgets # fix this import
from ipywidgets import Layout, Box
from IPython.display import clear_output
import functools

from lhcsmapi.dbsignal.post_mortem.PmDbSignal import PmDbSignal
from lhcsmapi.dbsignal.cals.CalsDbSignal import CalsDbSignal
from lhcsmapi.dbsignal.SignalUtilities import SignalUtilities
from lhcsmapi.Time import Time


class QuerySaveToCsvModule(object):
    
    def __init__(self, _get_signal_list=None, _out=None):
        self.out = _out
        
        self.btn_query = widgets.Button(description="Query", layout=Layout(width='15%'))
        
        self.btn_query.on_click(functools.partial(QuerySaveToCsvModule.on_btn_querry_clicked, self=self))
        
        self.btn_save = widgets.Button(description="Save to csv", layout=Layout(width='15%'))
        
        self.btn_save.on_click(functools.partial(QuerySaveToCsvModule.on_btn_save_clicked, self=self))        
        self.lbl_query = widgets.Label(value="")
        self.progress_querry = widgets.IntProgress(value=0, min=0, step=1, bar_style='success', orientation='horizontal',
                                          layout=Layout(width='20%'))
                
        self.query_list = []
        self.index_output = 0
        
        self.get_signal_list = _get_signal_list
                
        self.widget = Box(children=[self.btn_query, self.btn_save, self.lbl_query, self.progress_querry])

    def get_widget(self):
        return self.widget
    
    def set_signal_list_method(self, get_signal_list):
        self.get_signal_list = get_signal_list
    
    @staticmethod
    def on_btn_querry_clicked(b, self):
        self.signal_list = self.get_signal_list()
        self.query_signals()
        with self.out:
            clear_output(True)
            self.plot_signals()
        
    def query_signals(self):
        self.query_output = []

        self.progress_querry.max = len(self.signal_list)
        self.progress_querry.description = '0/{}'.format(len(self.signal_list))

        is_query_homogeneous = all([type(sig) == type(self.signal_list[0]) for sig in self.signal_list])
        # find smallest start time for CALS
        t0_sync = self.find_smallest_cals_signal_start_time()

        for i, sl in enumerate(self.signal_list):
            if type(self.signal_list[i]) is PmDbSignal:
                self.lbl_query.value = 'Querying signal: {}, source: {}'.format(sl.SignalName, sl.Source)
                t0 = sl.EventTime if is_query_homogeneous else int(t0_sync*1e9)
                unit = 'ns'
            elif type(sl) is CalsDbSignal:
                self.lbl_query.value = 'Querying signal: {}, '.format(sl.Name)
                t0 = None if is_query_homogeneous else t0_sync
                unit = 's'
            else:
                raise KeyError("Signal type name {} not supported.".format(type(sl)))

            df = self.signal_list[i].read()
            dfSync = SignalUtilities.synchronizeDataframe(df, t0=t0)
            dfSyncSec = SignalUtilities.convertIndicesToSeconds(dfSync, unit=unit)
            self.query_output.append(dfSyncSec)

            # on query complete
            self.progress_querry.value = i + 1
            self.progress_querry.description = '{}/{}'.format(i + 1, len(self.signal_list))

        self.lbl_query.value = 'Query completed.'

    def find_smallest_cals_signal_start_time(self):
        t0_sync = 0
        for sl in self.signal_list:
            if type(sl) is CalsDbSignal:
                t0_sync = min(t0_sync, -Time.convertToUnixTimestamp(sl.StartTime))
        return -t0_sync
            
    def plot_signals(self):
        legends = []
        axes = None
        for i, sl in enumerate(self.signal_list):
            if type(sl) is PmDbSignal:
                legends.append('Signal: {}, source: {}'.format(sl.SignalName, sl.Source))
            elif type(sl) is CalsDbSignal:
                legends.append('Signal: {}, '.format(sl.Name))
            else:
                raise KeyError("Signal type name {} not supported.".format(type(sl)))

            if axes:
                self.query_output[i].plot(figsize=(10, 7), ax=axes, grid=True)
            else:
                axes = self.query_output[i].plot(figsize=(10, 7), grid=True)
        axes.legend(legends, loc='upper right')
    
    @staticmethod
    def on_btn_save_clicked(b, self):
        self.progress_querry.max = len(self.query_output)
        for i, sl in enumerate(self.signal_list):
            if type(sl) is PmDbSignal:
                file_name = '{}_{}_{}.csv'.format(self.index_output, sl.Source,
                                                 sl.SignalName.replace(':', '_'))
            elif type(sl) is CalsDbSignal:
                file_name = '{}_{}.csv'.format(self.index_output, sl.Name[0].replace(':', '_'))
            self.progress_querry.value = i + 1
            self.progress_querry.description = '{}/{}'.format(i + 1, len(self.query_output))
            self.index_output += 1
            self.query_output[i].to_csv(file_name)
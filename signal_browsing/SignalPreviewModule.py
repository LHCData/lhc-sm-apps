import ipywidgets as widgets
from ipywidgets import Layout
from ipywidgets import Box, VBox
import functools
from lhcsmapi.post_mortem.PmDbRequest import PmDbRequest
from lhcsmapi.post_mortem.PmDbSignal import PmDbSignal
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

from lhcsmapi.cals.CalsDbSignal import CalsDbSignal

class SignalPreviewModule(object):
    def __init__(self, get_start_time=None, get_end_time=None, get_state=None, ldb=None):
        
        self.btn_sn = widgets.Button(description="Add Signal", layout=Layout(width='20%'))
        self.btn_sn.on_click(functools.partial(SignalPreviewModule.on_btn_sn_clicked, self=self))

        self.lbl_out = widgets.Label(value="")
        self.progress_search = widgets.IntProgress(value=0, min=0, step=1, bar_style='success', 
                                                   orientation='horizontal', layout=Layout(width='20%'))
        
        self.signal_list = []
        self.get_state = get_state
        self.get_start_time = get_start_time
        self.get_end_time = get_end_time
        self.ldb = ldb

        box_add = Box(children=[self.btn_sn, self.lbl_out, self.progress_search])
                
        # Overview of signals to be queried
        self.selmul_query = widgets.SelectMultiple(
            options=[' ' * 100],
            description='Query',
            layout=Layout(display="flex", flex_flow='column', width='100%', height='250px')
        )
        
        self.btn_rmv = widgets.Button(description="Remove Selected", layout=Layout(width='15%'))
        self.btn_rmv.on_click(functools.partial(SignalPreviewModule.on_btn_rmv_clicked, self=self))

        self.widget = VBox([box_add, self.selmul_query, self.btn_rmv])
        
    def get_widget(self):
        return self.widget
    
    def get_signal_list(self):
        return self.signal_list

    @staticmethod
    def on_btn_sn_clicked(b, self):
        self.add_signals()
    
    def add_signals(self):
        state = self.get_state()
        if len(state.cirname) > 0:
            start_time = self.get_start_time()
            end_time = self.get_end_time()

            signal_names_in = SignalMetadata.getVariableName(state.cir, state.cirname, state.sys, state.db,
                                                           state.sig)

            if state.db == 'PM':
                # add pm queries
                pm_metadata = SignalMetadata.getCircuitVariableDatabaseMetadata(state.cir, state.cirname[0],
                                                                               state.sys, state.db)
                system = pm_metadata["system"]
                class_name = pm_metadata["className"]
                source = pm_metadata["source"]
                # find PM data
                source = SignalMetadata.updatePMSourceWithWildcard(source, state.cir, state.sys, state.sig, state.wildcard)
                signal_names = SignalMetadata.updatePMSignalNamesWithWildcard(signal_names_in, state.sys, state.sig, state.wildcard)

                # SignalMetadata should always return a list
                source = source if type(source) is list else [source]

                self.lbl_out.value = 'Searching for PM events'
                self.progress_search.max = len(source)

                # PmDbRequest can search over periods of time larger than a day
                for i in range(len(source)):
                    self.lbl_out.value = 'Searching for PM entries, source: {}'.format(source[i])
                    self.progress_search.value = i + 1
                    event_source_and_timestamp = PmDbRequest.findEventsBetweenDates(start_time, end_time, class_name, source[i], system)

                for source, timestamp in event_source_and_timestamp:
                    for signal_name in signal_names:
                        self.signal_list.append(PmDbSignal().buildSignal(system=system, className=class_name, source=source,
                                                     signalName=signal_name, eventTime=timestamp))
                self.lbl_out.value = 'Completed search for PM events'

            elif state.db == 'CALS':
                signal_names = SignalMetadata.updateCALSSignalNamesWithWildcard(signal_names_in, state.sys, state.wildcard)
                for signal_name in signal_names:
                    self.signal_list.append(CalsDbSignal.buildSignal(name=signal_name, startTime=start_time,
                                                               endTime=end_time, ldb=self.ldb))
            else:
                raise KeyError("Database type {} not present in {}.".format(state.db, ['PM', 'CALS']))

            # update the query list
            self.update_query_list()

    def update_query_list(self):
        self.query_list = []
        for sl in self.signal_list:
            self.query_list.append(sl.toString())

        with self.selmul_query.hold_trait_notifications():
            self.selmul_query.options = self.query_list   

    @staticmethod
    def on_btn_rmv_clicked(b, self):
        # Query all, update progress bar and list
        if len(self.signal_list) > 0:
            selected = list(self.selmul_query.index)
            selected.reverse()
            for i in selected:
                self.signal_list.pop(i)
            self.update_query_list()
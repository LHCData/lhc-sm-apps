import ipywidgets as widgets
from ipywidgets import Layout
from ipywidgets import Box
import functools
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

class SignalMetadataBrowserModule(object):
    class State(object):
        def __init__(self, widget):
            if widget.sel_cir.index is None:
                widget.sel_cir.index = 0
            self.cir = widget.sel_cir.options[widget.sel_cir.index]

            if widget.sel_sys.index is None:
                widget.sel_sys.index = 0
            self.sys = widget.sel_sys.options[widget.sel_sys.index]

            if widget.sel_db.index is None:
                widget.sel_db.index = 0
            self.db = widget.sel_db.options[widget.sel_db.index]

            if widget.sel_sig.index is None:
                widget.sel_sig.index = 0
            self.sig = widget.sel_sig.options[widget.sel_sig.index]

            if len(widget.selmul_cirname.index) == 0:
                widget.selmul_cirname.index = (0,)
            self.cirname = list(widget.selmul_cirname.value)

            if len(widget.selmul_wildcard.index) != 0:
                self.wildcard = list(widget.selmul_wildcard.value)
            else:
                self.wildcard = []
    
    def __init__(self):
        # Initialize widgets
        circuitTypes = SignalMetadata.getCircuitTypes()
        systemTypes = SignalMetadata.getSystemTypesPerCircuit(circuitTypes[0])
        databases = SignalMetadata.getDatabaseNames(circuitTypes[0], systemTypes[0])
        circuitNames = SignalMetadata.getCircuitNames(circuitTypes[0])
        signals = SignalMetadata.getVariableNames(circuitTypes[0], circuitNames[0], systemTypes[0], databases[0])
        
        # Singular Selection
        # # Circuit
        self.sel_cir = widgets.Select(
            options=circuitTypes, description='Type', 
            layout=Layout(display="flex", flex_flow='column', width='15%')
        )        
        self.sel_cir.observe(functools.partial(SignalMetadataBrowserModule.on_value_change_sel_cir, self=self), names='value')
        
        # # System
        self.sel_sys = widgets.Select(
            options=systemTypes, description='System', 
            layout=Layout(display="flex", flex_flow='column', width='15%')
        )
        self.sel_sys.observe(functools.partial(SignalMetadataBrowserModule.on_value_change_sel_sys, self=self), names='value')
        
        # # Database
        self.sel_db = widgets.Select(
            options=databases, description='Database', 
            layout=Layout(display="flex", flex_flow='column', width='10%')
        )
        self.sel_db.observe(functools.partial(SignalMetadataBrowserModule.on_value_change_sel_db, self=self), names='value')
        
        # # Signal
        self.sel_sig = widgets.Select(
            options=signals, description='Signal', 
            layout=Layout(display="flex", flex_flow='column', width='20%')
        )
        self.sel_sig.observe(functools.partial(SignalMetadataBrowserModule.on_value_change_sel_sig, self=self), names='value')
        
        # Multiple Selection
        # # Circuit Name
        self.selmul_cirname = widgets.SelectMultiple(
        options=circuitNames, description='Circuit Name', value=[circuitNames[0]], 
            layout=Layout(display="flex", flex_flow='column', width='15%')
        )
        self.selmul_cirname.observe(functools.partial(SignalMetadataBrowserModule.on_value_change_selmul_cirname, self=self), names='value')
        
        # # Wildcard
        self.layout_hidden = widgets.Layout(display="flex", flex_flow='column', width='15%', visibility='hidden')
        self.layout_visible = widgets.Layout(display="flex", flex_flow='column', width='15%', visibility='visible')
        
        self.selmul_wildcard = widgets.SelectMultiple(
            options=[], rows=15, layout=self.layout_hidden
        )
        
        # Put together selection widgets
        self.widget = Box(children=[self.sel_cir, self.sel_sys, self.sel_db, self.sel_sig, self.selmul_cirname, self.selmul_wildcard])
        
    def get_widget(self):
        return self.widget
    
    def get_state(self):
        return SignalMetadataBrowserModule.State(self)
        
    @staticmethod
    def on_value_change_sel_cir(change, self):
        self.update_sel_cir()

    def update_sel_cir(self):
        with self.selmul_cirname.hold_trait_notifications():
            self.selmul_cirname.options = SignalMetadata.getCircuitNames(SignalMetadataBrowserModule.State(self).cir)
            self.selmul_cirname.index = (0,)
        with self.sel_sys.hold_trait_notifications():
            self.sel_sys.index = None
            self.sel_sys.options = SignalMetadata.getSystemTypesPerCircuit(SignalMetadataBrowserModule.State(self).cir)
            self.sel_sys.index = 0

        self.update_sel_sys()

    @staticmethod
    def on_value_change_sel_sys(change, self):
        self.update_sel_sys()

    def update_sel_sys(self):
        with self.sel_db.hold_trait_notifications():
            self.sel_db.options = SignalMetadata.getDatabaseNames(SignalMetadataBrowserModule.State(self).cir, 
                                                                  SignalMetadataBrowserModule.State(self).sys)
            self.sel_db.index = 0
        self.update_sel_db()

    @staticmethod
    def on_value_change_sel_db(change, self):
        self.update_sel_db()

    def update_sel_db(self):
        with self.sel_sig.hold_trait_notifications():
            cir_names = SignalMetadata.getCircuitNames(SignalMetadataBrowserModule.State(self).cir)
            self.sel_sig.options = SignalMetadata.getVariableNames(SignalMetadataBrowserModule.State(self).cir, 
                                                              cir_names[0], 
                                                              SignalMetadataBrowserModule.State(self).sys, 
                                                              SignalMetadataBrowserModule.State(self).db)
            self.sel_sig.index = 0
            self.update_sel_sig()

    @staticmethod
    def on_value_change_sel_sig(change, self):
        self.update_sel_sig()

    def update_sel_sig(self):
        self.update_wildcard()

    def update_wildcard(self):
        wildcardNameAndValue = SignalMetadata.getWildcardForCircuitSystemDbSignal(SignalMetadataBrowserModule.State(self).cir, 
                                                                                  SignalMetadataBrowserModule.State(self).cirname,
                                                                                  SignalMetadataBrowserModule.State(self).sys, 
                                                                                  SignalMetadataBrowserModule.State(self).db, 
                                                                                  SignalMetadataBrowserModule.State(self).sig)
        if len(wildcardNameAndValue) == 0:
            self.selmul_wildcard.layout = self.layout_hidden
        else:
            self.selmul_wildcard.layout = self.layout_visible
            wildcardName, wildcardValues = wildcardNameAndValue # unpack tuple
            with self.selmul_wildcard.hold_trait_notifications():
                self.selmul_wildcard.description = wildcardName
                self.selmul_wildcard.options = wildcardValues

    @staticmethod
    def on_value_change_selmul_cirname(change, self):
        self.update_wildcard()
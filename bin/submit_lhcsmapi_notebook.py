#!/usr/bin/env python
import sys, json, os, re
import argparse
import subprocess
import tempfile
import shutil

CVMFS_HADOOP_CONFEXT = "/cvmfs/sft.cern.ch/lcg/etc/hadoop-confext"
CVMFS_LCG_VIEW = "/cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt"


def parse_swan_notebook_line(value):
    value = value.rstrip()
    if value.startswith("%"):
        return value.replace("%", "# ")
    return value


def run_commands(commands):
    parsed_commands = '; '.join(commands)
    print('Running: %s' % parsed_commands)
    print('')

    p = subprocess.Popen(parsed_commands, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True, universal_newlines=True)
    for line in p.stdout:
        print(line.rstrip())

    return p.poll()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--appid", dest="appid" ,required=True, help="spark application id of the job")
    parser.add_argument("--ipynb", dest="ipynb", required=True, help="path to spark notebook .ipynb file")
    parser.add_argument('--conf', dest="customconf", action='append', help="additional spark configuration")
    args = parser.parse_args()

    input_ipynb_file_path = os.path.abspath(args.ipynb)
    input_bundles_file_path = os.path.join(CVMFS_HADOOP_CONFEXT, 'bundles', 'bundles.json')
    hadoop_swan_setconf = os.path.join(CVMFS_HADOOP_CONFEXT, 'hadoop-swan-setconf.sh')
    
    output_py_file = "/tmp/%s.py" % (args.appid)

    try:

        print('Converting %s notebook code to output python file %s ..' % (input_ipynb_file_path, output_py_file))
        notebook = json.load(open(input_ipynb_file_path))

        # parse configs
        code_lines = [
            # initialize spark config
            'from pyspark import SparkConf, SparkContext',
            'from pyspark.sql import SparkSession',
            'conf = SparkConf()',
        ]

        # start spark session
        code_lines.append('')
        code_lines.append('sc = SparkContext(conf=conf)')
        code_lines.append('spark = SparkSession(sc)')
        code_lines.append('')

        # this is needed in order to use headless mode for the notebooks which display something
        code_lines.append('import matplotlib')
        code_lines.append('matplotlib.use("Agg")')

        # add actual notebook cells
        for cell in notebook['cells']:
            if cell['cell_type'] == 'code':
                for line in cell['source']:
                    code_lines.append(parse_swan_notebook_line(line))

        # if notebook succeedes, success should be printed
        code_lines.append('')
        code_lines.append('sc.stop()')
        code_lines.append('print("SUCCESS")')

        with open(output_py_file, "w+") as f:
            f.write('\n'.join(code_lines))

        # parse notebook sparkconnect bundled_options options
        print('Parsing swan notebook spark configuration for nxcals bundle file %s ..' % input_bundles_file_path)
        bundles = json.load(open(input_bundles_file_path))
        custom_spark_submit_flags = ""
        for option in bundles['bundled_options']['NXCALS']['options']:
            # convert swan {} to bash $ for env variables
            option_value = re.sub(r"{(.*?)}", r"$\1", option['value'])

            # adjust truststore.jks path in spark option as in cluster mode
            # this file will be in working dir of the executor and driver
            option_value = option_value.replace("/etc/pki/tls/certs/truststore.jks", "./truststore.jks")

            custom_spark_submit_flags = '%s --conf %s="%s"' % (
                custom_spark_submit_flags, option['name'], option_value
            )
        
        if args.customconf:
            for customconf in args.customconf:
                custom_spark_submit_flags = '%s --conf %s' % (
                    custom_spark_submit_flags, customconf
                )
        
        # NXCALS requires truststore.jks, generate one in /tmp/truststore.jks
        if not os.path.exists("/tmp/truststore.jks"):
            run_commands([
                'source %s/setup.sh' % (CVMFS_LCG_VIEW),
                'keytool -import -alias cerngridCA -file /etc/pki/tls/certs/CERN_Grid_Certification_Authority.crt -keystore /tmp/truststore.jks -storepass password -noprompt',
                'keytool -import -alias cernRootCA2 -file /etc/pki/tls/certs/CERN_Root_Certification_Authority_2.crt -keystore /tmp/truststore.jks -storepass password -noprompt',
            ])

        # We require LHCSMAPI (temporary, as lhcsmapi sould be in CVMFS)
        if not os.path.exists("/tmp/lhcsmapi.zip"):
            run_commands([
               'pip install -t /tmp/lhcsmapi lhcsmapi',
               'pip install -t /tmp/lhcsmapi tzlocal',
               'pip install -t /tmp/lhcsmapi influxdb',
            ])
            shutil.make_archive("/tmp/lhcsmapi", 'zip', "/tmp/lhcsmapi")
        
        print('Executing swan spark notebook as batch job ..')

        rc = run_commands([
            'set -e',
            # setup spark environemnt and nxcals cluster configuration
            'source %s/setup.sh' % (CVMFS_LCG_VIEW),
            'export LCG_VIEW=%s' % (CVMFS_LCG_VIEW),
            'source %s %s || (echo "sourcing config. failed"; exit 1)' % (hadoop_swan_setconf, 'nxcals'),
            # make sure tokens are not set
            'unset HADOOP_TOKEN_FILE_LOCATION',
            # submit spark application
            'spark-submit ' +
                # general yarn flags
                '--deploy-mode cluster --master yarn ' +
                '--conf spark.app.name="%s" ' % args.appid +
                '--conf spark.yarn.submit.waitAppCompletion=false ' +
                '--conf spark.yarn.dist.archives=/tmp/lhcsmapi.zip#dist_lhcsmapi ' +
                # align executors and driver libraries environment
                '--conf spark.yarn.appMasterEnv.PYTHONPATH=dist_lhcsmapi:$PYTHONPATH ' +
                '--conf spark.yarn.appMasterEnv.LD_LIBRARY_PATH=$LD_LIBRARY_PATH ' +
                '--conf spark.executorEnv.LD_LIBRARY_PATH=$LD_LIBRARY_PATH ' +
                # nxcals requires kerberos ticket and truststore.jks to be available in the driver, distribute
                '--conf spark.yarn.dist.files="$KRB5CCNAME#krbcache,/tmp/truststore.jks#truststore.jks" ' +
                '--conf spark.yarn.appMasterEnv.KRB5CCNAME="./krbcache" ' +
                '%s %s' % (custom_spark_submit_flags, output_py_file),
        ])

        # exit with return code of the notebook
        exit(rc)
    except Exception as e:
        print("WRONG NOTEBOOK PROVIDED OR ENVIRONMENT INVALID (error %s)" % e)
        print(e)
        exit(2)

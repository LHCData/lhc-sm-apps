{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# BEAM LOSS MONITOR QUERY with pyeDSL\n",
    "\n",
    "The goal of this blog entry is to demontrate the use of the pyeDSL to conventiently query BLM-related data from Post Mortem (PM) and NXCALS databases.\n",
    "In particular, we will show how to query for a given period of time around an interesting event in the LHC:\n",
    "- LHC context information from PM\n",
    "- BLM signals from PM for plotting and extraction of statistical features (min, max, etc.)\n",
    "- BLM signals from NXCALS for extraction of statistical features (min, max, etc.)\n",
    "\n",
    "The PM queries presented below are partially based on previous work of:\n",
    "- M. Valette: https://gitlab.cern.ch/LHCData/PostMortemData\n",
    "- M. Dziadosz, R. Schmidt, L. Grob: https://gitlab.cern.ch/UFO/PMdataAnalysis\n",
    "- P. Belanger: https://gitlab.cern.ch/pbelange/UFOBuster\n",
    "\n",
    "We found useful a presentation on BLM logging:\n",
    "https://indico.cern.ch/event/20366/contributions/394830/attachments/307939/429949/BLM_6th_Radiation_Workshop_Christos.pdf\n",
    "\n",
    "Description of BLM running sums: https://ab-div-bdi-bl-blm.web.cern.ch/Acquisition_system/Data_acquisition_integration_durations_20100313.htm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 0. Import Necessary Packages\n",
    "- **Time** is a class for time manipulation and conversion\n",
    "- **Timer** is a class for measuring time of code execution\n",
    "- **QueryBuilder** is a pyeDSL class for a construction of database queries\n",
    "- **PmDbRequest** is a low-level class for executing PM queries\n",
    "- **MappingMetadata** is a class for retrieving BLM table names for NXCALS query\n",
    "- **BlmAnalysis** class provides some helper functions for BLM Analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from lhcsmapi.Time import Time\n",
    "from lhcsmapi.Timer import Timer\n",
    "from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder\n",
    "from lhcsmapi.pyedsl.FeatureBuilder import FeatureBuilder\n",
    "from lhcsmapi.dbsignal.post_mortem.PmDbRequest import PmDbRequest\n",
    "from lhcsmapi.metadata.MappingMetadata import MappingMetadata\n",
    "import lhcsmapi.analysis.BlmAnalysis as BlmAnalysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 0.1. LHCSMAPI version"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'1.4.35'"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import lhcsmapi\n",
    "lhcsmapi.__version__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. User Input\n",
    "We choose as a start date a beam dump in the LHC. In this case, PM stores a data event with information about LHC context and BLM signals. In order to find PM events, we look one second before and two seconds after the selected beam dump.\n",
    "\n",
    "NXCALS database performs a continuous logging of BLM signals (with various running sums). Thus, NXCALS can be queried any time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "start_time_date = '2015-11-23 07:28:53+01:00'\n",
    "t_start, t_end = Time.get_query_period_in_unix_time(start_time_date=start_time_date, duration_date=[(1, 's'), (2, 's')])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. LHC Context\n",
    "In this part we query LHC context information in order to support BLM analysis. To this end, we employ context_query provided by pyeDSL.\n",
    "- The context_query() works only in a general-purpose query mode with metadata provided by with_query_parameters() method. Considering the general-purpose mode, the user has to provide system, className, and source.\n",
    "\n",
    "For the source field one can use a wildcard ('\\*'). However, the query takes more time as compared to a one where the BLM source is specified. The list of available BLM source can be queried from PM as showed in the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "http://pm-api-pro/v2/pmdata/within/duration?system=BLM&className=BLMLHC&source=*&fromTimestampInNanos=1448260132000000000&durationInNanos=2000000000\n",
      "Elapsed: 22.240 s.\n"
     ]
    }
   ],
   "source": [
    "with Timer():\n",
    "    QueryBuilder().with_pm() \\\n",
    "        .with_duration(t_start=t_start, duration=[(2, 's')]) \\\n",
    "        .with_query_parameters(system='BLM', className='BLMLHC', source='*') \\\n",
    "        .context_query(contexts=[\"pmFillNum\"]).df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "http://pm-api-pro/v2/pmdata/within/duration?system=BLM&className=BLMLHC&source=HC.BLM.SR6.C&fromTimestampInNanos=1448260132000000000&durationInNanos=2000000000\n",
      "Elapsed: 1.434 s.\n"
     ]
    }
   ],
   "source": [
    "with Timer():\n",
    "    QueryBuilder().with_pm() \\\n",
    "        .with_duration(t_start=t_start, duration=[(2, 's')]) \\\n",
    "        .with_query_parameters(system='BLM', className='BLMLHC', source='HC.BLM.SR6.C') \\\n",
    "        .context_query(contexts=[\"pmFillNum\"]).df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, for the remaining context queries we always provide the source."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "http://pm-api-pro/v2/pmdata/within/duration?system=LHC&className=CISX&source=CISX.CCR.LHC.A&fromTimestampInNanos=1448260132000000000&durationInNanos=2000000000\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>OVERALL_ENERGY</th>\n",
       "      <th>OVERALL_INTENSITY_1</th>\n",
       "      <th>OVERALL_INTENSITY_2</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>1448260133517488525</th>\n",
       "      <td>20915</td>\n",
       "      <td>16537</td>\n",
       "      <td>17110</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                     OVERALL_ENERGY  OVERALL_INTENSITY_1  OVERALL_INTENSITY_2\n",
       "1448260133517488525           20915                16537                17110"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=t_start, duration=[(2, 's')]) \\\n",
    "    .with_query_parameters(system='LHC', className='CISX', source='CISX.CCR.LHC.A') \\\n",
    "    .context_query(contexts=[\"OVERALL_ENERGY\", \"OVERALL_INTENSITY_1\", \"OVERALL_INTENSITY_2\"]).df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=t_start, duration=[(2, 's')]) \\\n",
    "    .with_query_parameters(system='LHC', className='CISX', source='CISX.CCR.LHC.GA') \\\n",
    "    .context_query(contexts=[\"BEAM_MODE\"]).df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "http://pm-api-pro/v2/pmdata/within/duration?system=LBDS&className=BSRA&source=LHC.BSRA.US45.B1&fromTimestampInNanos=1448260132000000000&durationInNanos=5000000000\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>aGXpocTotalIntensity</th>\n",
       "      <th>aGXpocTotalMaxIntensity</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>1448260134624467000</th>\n",
       "      <td>2.457193e+10</td>\n",
       "      <td>8.560893e+08</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                     aGXpocTotalIntensity  aGXpocTotalMaxIntensity\n",
       "1448260134624467000          2.457193e+10             8.560893e+08"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=t_start, duration=[(5, 's')]) \\\n",
    "    .with_query_parameters(system='LBDS', className='BSRA', source='LHC.BSRA.US45.B1') \\\n",
    "    .context_query(contexts=['aGXpocTotalIntensity', 'aGXpocTotalMaxIntensity']).df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=t_start, duration=[(5, 's')]) \\\n",
    "    .with_query_parameters(system='LBDS', className='BSRA', source='LHC.BSRA.US45.B2') \\\n",
    "    .context_query(contexts=['aGXpocTotalIntensity', 'aGXpocTotalMaxIntensity']).df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also developed a method for getting the LHC context shown above with a single method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lhc_context_df = BlmAnalysis.get_lhc_context(start_time_date)\n",
    "lhc_context_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Query Single BLM\n",
    "Once the LHC context is obtained, we move on to the query of BML signals with PM and NXCALS.\n",
    "## 3.1. Post Mortem\n",
    "The first step is to find the list of PM event sources. To this end, we employ the pyeDSL."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "source_timestamp_df = QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=start_time_date, duration=[(1, 's'), (2, 's')]) \\\n",
    "    .with_query_parameters(system='BLM', className='BLMLHC', source='*') \\\n",
    "    .event_query(display_warning=True).df\n",
    "\n",
    "source_timestamp_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1.1. List of Beam Loss Monitors\n",
    "Then, we choose one BLM cluster (HC.BLM.SR6.C) and check the names of actual BLMs it contains. For the sake of completeness, the timestamp (although the same) is also provided."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blm_names_df = QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=t_start, duration=[(2, 's')]) \\\n",
    "    .with_query_parameters(system='BLM', className='BLMLHC', source='HC.BLM.SR6.C') \\\n",
    "    .context_query(contexts=[\"blmNames\"]).df\n",
    "blm_names_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The list of available variables for a PM event is accessed through a low-level lhcsmapi call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response_blm = PmDbRequest.get_response(\"pmdata\", False, True, pm_rest_api_path=\"http://pm-api-pro/v2/\", \n",
    "                                        system='BLM', className='BLMLHC', source='HC.BLM.SR6.C',\n",
    "                                        fromTimestampInNanos=t_start, durationInNanos=int(2e9))\n",
    "for entry in response_blm['content'][0]['namesAndValues']:\n",
    "    print(entry['name'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lhc_parameters_df = PmDbRequest.get_response(\"pmdata\", False, True, pm_rest_api_path=\"http://pm-api-pro/v2/\", \n",
    "                                        system='LHC', className='CISX', source='CISX.CCR.LHC.A',\n",
    "                                        fromTimestampInNanos=t_start, durationInNanos=int(2e9))\n",
    "for entry in lhc_parameters_df['content'][0]['namesAndValues']:\n",
    "    print(entry['name'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lhc_bm_df = PmDbRequest.get_response(\"pmdata\", False, True, pm_rest_api_path=\"http://pm-api-pro/v2/\", \n",
    "                                        system='LHC', className='CISX', source='CISX.CCR.LHC.GA',\n",
    "                                        fromTimestampInNanos=t_start, durationInNanos=int(2e9))\n",
    "for entry in lhc_bm_df['content'][0]['namesAndValues']:\n",
    "    print(entry['name'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "abort_gap_b1_df = PmDbRequest.get_response(\"pmdata\", False, True, pm_rest_api_path=\"http://pm-api-pro/v2/\", \n",
    "                                        system='LBDS', className='BSRA', source='LHC.BSRA.US45.B1',\n",
    "                                        fromTimestampInNanos=t_start, durationInNanos=int(5e9))\n",
    "for entry in abort_gap_b1_df['content'][0]['namesAndValues']:\n",
    "    print(entry['name'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "abort_gap_b2_df = PmDbRequest.get_response(\"pmdata\", False, True, pm_rest_api_path=\"http://pm-api-pro/v2/\", \n",
    "                                        system='LBDS', className='BSRA', source='LHC.BSRA.US45.B2',\n",
    "                                        fromTimestampInNanos=t_start, durationInNanos=int(5e9))\n",
    "for entry in abort_gap_b2_df['content'][0]['namesAndValues']:\n",
    "    print(entry['name'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will query several variables with pyeDSL. Note that pyeDSL does not support yet vectorial definition of signal names for this type of queries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "blm_log_history_df = QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=t_start, duration=[(2, 's')]) \\\n",
    "    .with_query_parameters(system='BLM', className='BLMLHC', source='HC.BLM.SR6.C', signal='pmLogHistory1310ms') \\\n",
    "    .signal_query() \\\n",
    "    .overwrite_sampling_time(t_sampling=4e-05, t_end=1) \\\n",
    "    .dfs[0]\n",
    "\n",
    "blm_thresholds_df = QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=t_start, duration=[(2, 's')]) \\\n",
    "    .with_query_parameters(system='BLM', className='BLMLHC', source='HC.BLM.SR6.C', signal='pmLogHistory1310msThresholds') \\\n",
    "    .signal_query() \\\n",
    "    .overwrite_sampling_time(t_sampling=4e-05, t_end=1) \\\n",
    "    .dfs[0]\n",
    "\n",
    "blm_turn_loss_df = QueryBuilder().with_pm() \\\n",
    "    .with_duration(t_start=t_start, duration=[(2, 's')]) \\\n",
    "    .with_query_parameters(system='BLM', className='BLMLHC', source='HC.BLM.SR6.C', signal='pmTurnLoss') \\\n",
    "    .signal_query() \\\n",
    "    .overwrite_sampling_time(t_sampling=4e-05, t_end=1) \\\n",
    "    .dfs[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Plot a single BLM"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "df = blm_turn_loss_df[blm_names_df.at[0, 'blmNames']]\n",
    "df = df[df.index > 1]\n",
    "df.plot(figsize=(15,7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.2. NXCALS\n",
    "In the next step we query NXCALS for BLM running sum 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Signal Query of Loss Running Sum 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 100 Hz data is stored locally in NFS (only available during run), Arek has an extractor for this data (potentially also 100 Hz data in NXCALS without 6 months legacy time; to be checked with Jakub Wozniak).\n",
    "# Vector-numeric data is present in logging.\n",
    "loss_rs01_df = QueryBuilder().with_nxcals(spark) \\\n",
    "    .with_duration(t_start=start_time_date, duration=[(100, 's'), (200, 's')]) \\\n",
    "    .with_query_parameters(nxcals_system='CMW', signal='%s:LOSS_RS01'%blm_names_df.at[0, 'blmNames']) \\\n",
    "    .signal_query() \\\n",
    "    .convert_index_to_sec() \\\n",
    "    .synchronize_time() \\\n",
    "    .dfs[0]\n",
    "\n",
    "loss_rs01_df.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Feature Query All BLMs for a Single Crate\n",
    "Due to the large number of BLMs, the query and plotting of them is impractical in this environment. In fact there are dedicated applications for this purpose. In the following, we will demonstrate the feature engineering (mean, std, min, max value) for the BLM signals stored in PM and NXCALS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "features = ['mean', 'std', 'max', 'min']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4.1. Post Mortem\n",
    "PM does not support calculation of features on the database (which is profitable from the communication and computation time perspective). Therefore, we need to query the raw signals and afterwards perform feature engineering.\n",
    "- pmLogHistory1310msThresholds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blm_thresholds_features_row_df = FeatureBuilder().with_multicolumn_signal(blm_thresholds_df) \\\n",
    "    .calculate_features(features=features, prefix='threshold') \\\n",
    "    .convert_into_row(index=lhc_context_df.index) \\\n",
    "    .dfs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- pmTurnLoss"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blm_turn_loss_features_row_df = FeatureBuilder().with_multicolumn_signal(blm_turn_loss_df) \\\n",
    "    .calculate_features(features=features, prefix='turn_loss') \\\n",
    "    .convert_into_row(index=lhc_context_df.index) \\\n",
    "    .dfs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4.2. NXCALS\n",
    "NXCALS ecosystem brings the cluster computing capabilities to the logging databases. It allows developing analysis code witht the Spark API. The pyeDSL encapsulates Spark API and provides a coherent feature engineering query. In other words, features are calculated on the cluster where the data is stored unlike the PM for which the calculation is performed locally.\n",
    "- Get signal names from MappingMetadata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blm_nxcals_df = MappingMetadata.get_blm_table()\n",
    "blm_nxcals_df['LOSS_RS01'] = blm_nxcals_df['Variable Name'].apply(lambda x: '%s:LOSS_RS01' % x)\n",
    "blm_nxcals_df['LOSS_RS09'] = blm_nxcals_df['Variable Name'].apply(lambda x: '%s:LOSS_RS09' % x)\n",
    "blm_nxcals_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Run a feature query with pyeDSL: Running Sum 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "loss_rs01_features_row_df = QueryBuilder().with_nxcals(spark) \\\n",
    "    .with_duration(t_start=start_time_date, duration=[(100, 's'), (200, 's')]) \\\n",
    "    .with_query_parameters(nxcals_system='CMW', signal=list(blm_nxcals_df['LOSS_RS01'])) \\\n",
    "    .feature_query(features=features) \\\n",
    "    .convert_into_row(lhc_context_df.index) \\\n",
    "    .df\n",
    "\n",
    "loss_rs01_features_row_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Run a feature query with pyeDSL: Running Sum 9"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "loss_rs09_features_row_df = QueryBuilder().with_nxcals(spark) \\\n",
    "    .with_duration(t_start=start_time_date, duration=[(100, 's'), (200, 's')]) \\\n",
    "    .with_query_parameters(nxcals_system='CMW', signal=list(blm_nxcals_df['LOSS_RS09'])) \\\n",
    "    .feature_query(features=features) \\\n",
    "    .convert_into_row(lhc_context_df.index) \\\n",
    "    .df\n",
    "\n",
    "loss_rs09_features_row_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5. Final Row\n",
    "Eventually, we put together all rows into a single one that can be stored in the persistent storage. The code of this notebook can be extracted into a job collecting historical data representing BLM signals during the operation of the LHC."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.concat([lhc_context_df, blm_thresholds_features_row_df, blm_turn_loss_features_row_df, loss_rs01_features_row_df, loss_rs09_features_row_df], axis=1)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  },
  "sparkconnect": {
   "bundled_options": [
    "NXCALS",
    "SparkMetrics",
    "DynamicAllocation"
   ],
   "list_of_options": []
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": false,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
